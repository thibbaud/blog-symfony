<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Post;
use App\Form\CommentType;
use App\Form\PostFormType;
use App\Repository\PostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PostController extends AbstractController
{
    /**
     * @Route("/post", name="post")
     */
    public function index(PostRepository $repo, Request $request)
    {
        $posts = $repo->findAll();
        return $this->render("post/index.html.twig", [
            "posts" => $posts
        ]);
    }

    /**
     * @param Post|null $post
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return RedirectResponse|Response
     * @Route("/post/create", name="post_create")
     * @Route("/post/{id]/edit", name="post_edit")
     */
    public function createPost(Post $post = null, Request $request, EntityManagerInterface $em) {
        if(!$post) {
            $post = new Post();
        }
        $form = $this->createForm(PostFormType::class, $post);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            if(!$post->getId()) {
                $post->setCreatedAt(new \DateTimeImmutable());
                $author = $this->getUser();
                $post->setAuthor($author);
            }
            $em->persist($post);
            $em->flush();

            return $this->redirectToRoute("post_view", [
                "id" => $post->getId()
            ]);
        }
        return $this->render("post/create.html.twig", [
            "form" => $form->createView()
        ]);
    }

    /**
     * @param Post $post
     * @param EntityManagerInterface $em
     * @param Request $request
     * @return Response
     * @Route("/post/{id}", name="post_view")
     */
    public function postView(Post $post, EntityManagerInterface $em, Request $request) {
        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $author = $this->getUser();
            $comment->setPost($post);
            $comment->setAuthor($author);
            $comment->setCreatedAt(new \DateTimeImmutable());
            $em->persist($comment);
            $em->flush();
            return $this->redirectToRoute("post_view", [
                "id" => $post->getId()
            ]);
        }

        return $this->render("post/postView.html.twig", [
            "post" => $post,
            "form" => $form->createView()
        ]);
    }
}
